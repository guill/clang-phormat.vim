" Settings
" These can be set globally by changing the g:var_name value
" They can be overwritten per-buffer by changing the b:var_name value
call phormat#option#register_default("phormat_command", "clang-format")
call phormat#option#register_default("phormat_sort_includes", 0)
call phormat#option#register_default("phormat_style", "file")
call phormat#option#register_default("phormat_fallback_style", "none")

call phormat#option#register_default("phormat_auto_on_save", 0)
call phormat#option#register_default("phormat_auto_on_save_valid_extensions", ['h','hpp','hxx','h++','c','cpp','c++','inc'])
call phormat#option#register_default("phormat_auto_on_save_total", 0)

" Commands
command! -range=% -nargs=0 ClangFormat call phormat#format#format_classic(<line1>,<line2>)
command! -range=% -nargs=0 ClangPhormat call phormat#format#format_partial(<line1>,<line2>)

augroup phormat-auto-group
	autocmd!
	autocmd BufWritePre * call phormat#auto#buf_write_pre()
augroup END
