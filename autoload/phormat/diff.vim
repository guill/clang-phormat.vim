function phormat#diff#get_modified_lines(bufnum) abort
	let l:diff_script = phormat#resource#get_path("getChangedLines.sh")
	let l:file_path = expand("#" . a:bufnum . ":p")
	let l:cmd = l:diff_script . ' '
	let l:cmd .= '"' . l:file_path . '" '
	let l:results_raw = systemlist(l:cmd, a:bufnum)
	let l:results = map(l:results_raw, {key, val -> split(val, ' ')})
	return l:results
endfunction
