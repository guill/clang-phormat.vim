function! phormat#auto#buf_write_pre()
	if !phormat#option#get("phormat_auto_on_save")
		return
	endif

	" Check valid extensions
	let l:valid_extensions = phormat#option#get("phormat_auto_on_save_valid_extensions")
	if index(l:valid_extensions, expand('%:e')) < 0
		return
	endif

	" Make it so that if someone undos the format and tries to save again, we
	" don't format
	let l:current_contents = getline(1,'$')
	if exists('b:phormat_prev_contents') && l:current_contents == b:phormat_prev_contents
		return
	endif
	let b:phormat_prev_contents = l:current_contents

	if phormat#option#get("phormat_auto_on_save_total")
		call phormat#format#format_classic(1,line('$'))
	else
		call phormat#format#format_partial(1,line('$'))
	endif
endfunction
