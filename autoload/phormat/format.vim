" Returns [contents, new_cursor_offset]
function! phormat#format#get_output(extra_args) abort
	let l:command = phormat#option#get("phormat_command")
	let l:command_args = phormat#format#get_args() . a:extra_args
	let l:results_raw = systemlist(l:command . ' ' . l:command_args . ' -', bufnr('%'))
	let l:cursor_info = matchlist(l:results_raw[0], '^{.*"Cursor": \([[:digit:]]\+\), "IncompleteFormat":.*}$')
	if len(l:cursor_info) < 2
		"No info on new cursor position
		return [l:results_raw]
	endif
	return [l:results_raw[1:], l:cursor_info[1]]
endfunction

function! phormat#format#get_args() abort
	let l:filename = expand('%:p')
	let l:byte_offset = line2byte(line('.')) + col('.') - 1
	"strings are falsy
	let l:fallback_style = phormat#option#get("phormat_fallback_style") 

	let l:args = ''
	let l:args .= '-assume-filename="' . l:filename . '" '
	let l:args .= '-cursor=' . l:byte_offset . ' '
	let l:args .= '-style="' . phormat#option#get("phormat_style") . '" '
	if type(l:fallback_style) == v:t_string
		let l:args .= '-fallback-style="' . l:fallback_style . '" '
	else
		let l:args .= '-fallback-style=none '
	endif
	if phormat#option#get("phormat_sort_includes")
		let l:args .= '-sort-includes '
	endif
	return l:args
endfunction

function! phormat#format#get_ranges_as_args(ranges)
	let l:args = ''
	for range in a:ranges
		let begin = range[0]
		let end = range[1]
		if l:end >= l:begin
			let l:args .= '-lines=' . begin . ':' . end . ' '
		endif
	endfor
	return l:args
endfunction

function! phormat#format#do_format(ranges)
	let l:range_args = phormat#format#get_ranges_as_args(a:ranges)
	let l:output_results = phormat#format#get_output(l:range_args)
	let l:contents = l:output_results[0]
	let l:oldview = winsaveview()
	let l:cursor_line_offset = get(l:oldview, "lnum") - get(l:oldview, "topline")
	if line('$') > len(l:contents)
		execute len(l:contents) . ',$delete _'
	endif
	call setline(1, l:contents)

	if len(l:output_results) <= 1
		call winrestview(l:oldview)
	else
		" Modify the window to keep cursor on the same line
		let l:oldview["topline"] = byte2line(l:output_results[1]) - l:cursor_line_offset
		call winrestview(l:oldview)
		execute 'goto ' . l:output_results[1]
	endif
endfunction

function! phormat#format#format_classic(start, end)
	call phormat#format#do_format([[a:start, a:end]])
endfunction

function! s:prune_chunks(chunks, start, end)
	return filter(map(a:chunks, {key, val -> [max([val[0],a:start]),min([val[1],a:end])]}), {key, val -> val[0] <= val[1]})
endfunction

function! phormat#format#format_partial(start, end)
	let l:modified_lines = phormat#diff#get_modified_lines(bufnr('%'))
	" Filter bad results
	let l:modified_lines = filter(l:modified_lines, {key, val -> len(val) > 0})
	" Lack of second argument is same as size 1
	let l:changed_chunks = map(l:modified_lines, {key, val -> [val[0], val[0] + get(val,1,1) - 1]})
	let l:pruned_chunks = s:prune_chunks(l:changed_chunks, a:start, a:end)
	if len(l:pruned_chunks) > 0
		call phormat#format#do_format(l:pruned_chunks)
	endif
endfunction
