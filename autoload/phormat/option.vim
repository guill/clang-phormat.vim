let s:default_options = {}

function! phormat#option#register_default(name, value)
	let s:default_options[a:name] = a:value
endfunction

function! phormat#option#get(name)
	if exists('b:' . a:name)
		return eval('b:' . a:name)
	endif
	if exists('g:' . a:name)
		return eval('g:' . a:name)
	endif
	if has_key(s:default_options, a:name)
		return get(s:default_options, a:name)
	endif
endfunction
