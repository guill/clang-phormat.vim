let s:my_path = fnamemodify(resolve(expand('<sfile>:p')), ':h')
function phormat#resource#get_path(resource_name) abort
	return resolve(s:my_path . '../../../resource/' . a:resource_name)
endfunction
