#!/bin/bash
FILEPATH="$1"

FILE_ABSPATH=$(realpath "$FILEPATH")
pushd $(dirname $FILE_ABSPATH) > /dev/null

TOPLEVEL=$(git rev-parse --show-toplevel)
FILE_RELPATH=$(realpath --relative-to="$TOPLEVEL" "$FILE_ABSPATH")

#echo "Relpath is $FILE_RELPATH"

FILE_BASENAME=$(basename -- "$FILE_RELPATH")
FILE_EXTENSION=${FILE_BASENAME##*.}
#echo "Extension is $FILE_EXTENSION"

TMP_FILEPATH=$(mktemp --suffix=".${FILE_EXTENSION}")
git show $2:"$FILE_RELPATH" > "$TMP_FILEPATH"
#echo "Tmpfile is $TMP_FILEPATH"

# sed extracts the changed line numbers from stdin
cat - | git diff --no-index --no-color -U0 -- "$TMP_FILEPATH" - | sed -n 's/^@@[^+]*+\([[:digit:]]\+\)\(,\([[:digit:]]\+\)\)\?.*@@.*$/\1 \3/p'

rm $TMP_FILEPATH
popd > /dev/null
